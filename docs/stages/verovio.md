
# Visualisation dynamique et responsive de partitions de musique

## Contexte

L'équipe Algomus développe [Dezrann](http://www.dezrann.net), une application
web pour lire et annoter des partitions musicales. Cette application utilise le
framework [Polymer](http://polymer-project.org) qui implémente la technologie
des *Web Components* (futur standard du W3C). Nous réalisons un ensemble de
balises HTML paramétrables qui s'intègrent aisément dans une page web à la
manière des balises HTML5 vidéo ou audio. Dezrann est utilisé d'un côté par des
classes de collèges pour découvrir la musique, et de l'autre, par des
musicologues annotant des corpus.

## Objectifs

Pour l'instant, notre rendu de partition est statique et limité.
L'objectif du projet est d'évaluer [Verovio](http://www.verovio.org), une
librairie javascript moderne pour afficher de la musique sous forme de MEI et
interagir avec elle.
Concrètement, le projet
 - commencera par utiliser Verovio sur des exemples simples,
 - puis tentera de construire un composant web prototype qui proposerait
 différentes options de récupération des positions voire d'interaction avec des
 notes ou d'autres objets de la partition.

En cas de succès, une intégration à Dezrann pourra être envisagée ainsi que des
tests avec nos utilisateurs.

*Mots clés: partitions musicales et analyse, synchronisation, animation,
Polymer, Web components, javascript.*

## Liens
- https://www.verovio.org
- http://www.dezrann.net
- http://polymer-project.org

## Bibliographie
- M. Giraud, R. Groult, E. Leguy, [Dezrann, a Web Framework to Share Music Analysis](https://hal.archives-ouvertes.fr/hal-01796787), TENOR 2018
- L. Pugin, R. Zitellini, P. Roland, Verovio, A Library for Engraving MEI Music Notation into SVG, ISMIR 2014
- L. Ma, M. Giraud, E. Leguy, Realtime collaborative annotation of music scores with Dezrann, CMMR 2019
