

# Default types and recommanded tags

The default types are defined in [dez-client/dez-analysis/lib/LabelTypes.js](https://gitlab.com/algomus.fr/dezrann/-/blob/dev/code/dez-client/dez-analysis/lib/LabelTypes.js).
Below are some recommendations. See also <http://www.algomus.fr/data> for some datasets encoded with these types,
notably the dataset of [Mozart String Quartets](https://gitlab.com/algomus.fr/algomus-data/tree/master/quartets/mozart) [(Allegraud 2019)](https://dx.doi.org/10.5334/tismir.27).


| `type`            | Recommended `tag`s                                  |
| ----------------- | --------------------------------------------------- |
| Pattern           | a, b, c, a1, a2, a', b'...                          |
| ----------------- | --------------------------------------------------- |
| Harmony           | C, Dm,                                              |
|                   | I, ii, III ... VII                                  |
|                   | I, V6, IV6, ii6/5, V7...  as in the  RomanText syntax [(Gotham 2019)](https://zenodo.org/record/3527756) |
| ----------------- | --------------------------------------------------- |
| Tonality          | C, Cm, ...                                          |
| ----------------- | --------------------------------------------------- |
| Cadence           | PAC, HC, DC, ...                                    |
|                   | I:PAC, V:PAC, ...                                   |
|                   | I:PAC, V:PAC, ...                                   |
|                   | V:PAC MC, I:PAC ECC, ... in a notation inspired by [Hepokoski and Darcy](https://en.wikipedia.org/wiki/Sonata_theory), as in [(Allegraud 2019)](https://dx.doi.org/10.5334/tismir.27)                             |
| ----------------- | --------------------------------------------------- |
| Pedal             | I, V, ...                                           |
| ----------------- | --------------------------------------------------- |
| Harmonic sequence | M, R1, R2...                                        |
| ----------------- | --------------------------------------------------- |
| Structure         | Exposition, Development, Recapitulation             |
|                   | Intro, Transition, Conclusion, Coda                 |
|                   | Theme, Variation                                    |
| ----------------- | ---------------------------------------             |
| Texture           | mel/acc (S/ATBh), as in [(Giraud 2014)](https://hal.archives-ouvertes.fr/hal-01057017) |
|                   | silence, unison, homorhythmy, parallel-move, hammer |




However, Dezrann is agnostic on these label types and values.
Any type/tag value can be styled through [`meta.layout` rules](../api/dez-format.md) in `.dez` files.

In any case, it is recommended to have:
- a controlled vocabulary for Types, with no more than 10-20 types
- a controlled syntax for tags, dependent on each type

Free text should not be written into tags but rather in comments.

