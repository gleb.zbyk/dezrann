Actuellement l'archi est un peu rigide à cause du fait qu'on y va à tâtons depuis le début et c'est bien normal. Les besoins sont beaucoup plus précis aujourd'hui, il est donc nécessaire de faire le point et de repartir sur de bonnes bases. Un point de départ qui me parait intéressant est de partir du markup et d'imaginer comment les composants peuvent être intégrés dans du code html.

**Une première proposition:**

```html
<dez-editor>
    <dez-header>
        <dez-menu></dez-menu>
        <dez-title></dez-title>
        <dez-toolbox></dez-toolbox>
    </dez-header>
    <dez-content>
        <dez-navigation>
            <!-- soit un composant sans affichage, ou sinon un argument de dez-navigation ou une variable globale ? ou renommer en dez-synchro ? -->
            <dez-audio audio="piece.mp3" synchro="synchro.json">
            </dez-audio>
            <!-- idem pour le réseau : composant autonome ou ailleurs ? -->
            <dez-collaborate>
            </dez-collaborate>

            <!-- composants affichés -->
            <dez-main>
                <dez-score image="score.png" positions="notes.json"></dez-score>
            </dez-main>
            <!-- et/ou -->
            <dez-main>
              <dez-wave image="wave.png" positions="positions.json"></dez-wave>
            </dez-main>
            <!--        -->
            <dez-minimap>
                <dez-score image="score.png" positions="notes.json" not-interactive></dez-score>
            </dez-minimap>

        </dez-navigation>
        <dez-label-editor></dez-label-editor>
    </dez-content>
</dez-editor>

```

Ceci n'est qu'une ébauche ou n'apparaissent ni les attributs (sauf pour dez-score) ni le shadow dom. Cette structure a l'avantage de regrouper les éléments qui ont besoin de communiquer entre eux. Par exemple les communications de navigation n'ont pas de sens en dehors de dez-navigation.

**dez-main**

C'est un conteneur qui permet de scroller horizontalement son contenu. Il peut exécuter des ordres de repositionnement venant de ses clients ou de signaux. Il communique tout changement de position.

Il prend toute la place disponible en largeur. Par défaut, il s'adapte à la hauteur du contenu.

Il est possible de fixer sa hauteur (attribut height); dans ce cas, il force la hauteur du contenu tout en préservant les proportions.

```html
<dez-main height="300">...</dez-main>
<dez-main zoom="0.5">...</dez-main>
```

Que faire des images très hautes? ex: partition à plus de 5 voix.


**dez-minimap**

C'est un conteneur. Il contient une vue réduite du premier contenu. Il en dépend donc. Une zone rectangulaire en surbrillance correspond à la partie visible du contenu principal. Cette zone peut être déplacée par l'utilisateur pour modifier la partie visible du contenu principal. `dez-minimap` communique donc sa position. Il exécute aussi des ordres de repositionnement venant de l'extérieur (ex: `dez-main`).

Il prend toute la place disponible en largeur. Il a une hauteur par défaut modifiable. La hauteur du contenu s'adapte. Le contenu est centré. Si le contenu est trop large, la zone de surbrillance sert d’ascenseur et est placée comme telle. Voir le comportement de la minimap atom ou sublimetext.

```html
<dez-minimap height="50">...</dez-main>
<dez-minimap zoom="0.2">...</dez-main>
```

**Les contenus**

Dans les conteneurs il est possible mettre divers contenus: une image par exemple ou ce qui nous intéresse plus, un composant métier comme `dez-score`. D'autre composants métier seront à terme créés: `dez-wave` (forme d'onde) et `dez-global-svg` (vue globale du schéma d'analyse).

A priori, dans `dez-main` et `dez-minimap` seront placés respectivement deux éléments de même type et compatibles pour que les communications aient un sens. Mais on peut imaginer avoir 2 éléments de types différents comme par exemple:

```html
<dez-main>
    <dez-score image="score.png" positions="notes.json"></dez-score>
</dez-main>
<dez-minimap>
    <dez-global-svg nb-staffs="3" nb-measures="123" not-interactive></dez-global-svg>
</dez-minimap>
```

**Le composant générique `dez-canvas`**

Graphiquement, les composants `dez-score`, `dez-global-svg` et `dez-wave` ont des comportements communs: ils permettent tous d'afficher et de manipuler des objets graphiques (globs) sur une image. Ces similarités sont factorisées dans le composant `dez-canvas`. Les 3 composants métier utilisent donc ce composant.

`dez-canvas` est donc composé d'un fond renseigné par l'attribut `bg`. Cela peut-être un fichier image ou du code svg.

```html
<dez-canvas bg="score.png"></dez-canvas>
<dez-canvas bg="<rect...\><text ...\>..."></dez-canvas>
```

Les globs sont positionnés selon une grille matérialisée par des zones (des lignes) et des abscisses relatives à ces zones.

```html
<dez-canvas bg="score.png" grid="grid.json"></dez-canvas>
```

Enfin, il est possible de charger des globs:

```html
<dez-canvas bg="score.png" grid="grid.json" globs="globs.json"></dez-canvas>
```

**dez-score**

`dez-score` possède un attribut `image` qui correspond à la partition de la pièce. L'attribut `position` peut être un fichier ou du code `json`:

```html
<dez-score image="score.png" positions="notes.json"></dez-score>

dez-canvas utilisé:

<dez-canvas bg="score.png" grid="grid.json"></dez-canvas>
```

Les positions sont présentées sous forme d'un tableau de portées, composées de mesures, composées de tuples `(abscisse, onset)`:

```html
<dez-score image="score.png" positions="[{bottom: $b, top: $t, measures: [[{x: $x, onset: $o}*]*}*]"></dez-score>

dez-canvas utilisé:

<dez-canvas bg="score.png" grid="[{bottom: $b, top: $t, positions: [$x*]}*]"></dez-canvas>
```

Il est possible de charger une analyse grâce à l'attribut `analysis`.

```html
<dez-score image="score.png" positions="notes.json" analysis="analysis.json"></dez-score>

dez-canvas utilisé:

<dez-canvas bg="score.png" grid="grid.json" globs="globs.json"></dez-canvas>
```

`analysis` est un tableau de labels:

```html
<dez-score
    image="score.png"
    positions="notes.json"
    analysis="reference=[musicaltime|piece.mp3],{type: $ty, tag: $ta, start: $s, duration: $d, staffs: [$num*], layers: [$id*]}*]"
></dez-score>

dez-canvas utilisé:

<dez-canvas
    bg="score.png"
    grid="grid.json"
    globs="{type: $ty, text: $te, x: $x, lg: $lg, color: $c, zones: [$id*]}*"
></dez-canvas>
```

Par exemple:

```html
<dez-score
    image="score.png"
    positions="notes.json"
    analysis="
        [
            {type: 'S', tag: 'Subject', start: 0.0, duration: 8.0, staffs: [1]},
            {type: 'cad', tag: 'PAC', start: 1.0, layers: [cadences]},
            {...}
        ]"
></dez-score>

dez-canvas utilisé:

<dez-canvas
    bg="score.png"
    grid="grid.json"
    globs="rect(2.0,10.0,'Subject','red') => 3 ... vline(3.0,'PAC','gray') => (3,4,5) ... triangle(12.0,'text') => 1"
></dez-canvas>
```

Les globs peuvent être exprimés en simple texte ou en json. Avec également un format textuel pour l'analyse:

```html
<dez-score
    image="score.png"
    positions="notes.json"
    analysis="S(0.0, 8.0, 'Subject') => 1 ... cad(1.0, 'tag') => cadences ... degree(12.0, 'V') => degrees"
></dez-score>

dez-canvas utilisé:

<dez-canvas
    bg="score.png"
    grid="grid.json"
    globs="rect(2.0,10.0,'Subject','red') => 3 ... vline(3.0,'PAC','gray') => (3,4,5) ... triangle(12.0,'text') => 1"
></dez-canvas>
```

Avec le texte dans le contenu du tag plutôt que dans l'attribut:

```html
<dez-score image="score.png" positions="notes.json">
    S(0.0, 8.0, 'Subject') CS1(10.5, 8.0, 'Counter subject 1') => 1
    cad(1.0, 'tag') => cadences
    degree(12.0, 'V') => degrees
</dez-score>

dez-canvas utilisé:

<dez-canvas bg="score.png" grid="grid.json">
    rect(2.0,10.0,'Subject','red') rect(14.0,9.0,'Counter subject','blue') => 3
    vline(3.0,'PAC','gray') => (3,4,5)
    triangle(12.0,'text') => 1
</dez-canvas>
```

**dez-global-svg**

A REDIGER

```html
<dez-global-svg nb-staffs="3" nb-measures="123"></dez-global-svg>
<!--measures 4/4 par défaut-->

dez-canvas utilisé:

<dez-canvas bg="$svg-code" grid="[{bottom: $b, top: $t, divisions: 123*4}*]"></dez-canvas>
```

```html
<dez-global-svg nb-staffs="3" nb-measures="123" measure="3/4"></dez-global-svg>

dez-canvas utilisé:

<dez-canvas bg="$svg-code" grid="[{bottom: $b, top: $t, divisions: 123*3}*]"></dez-canvas>
```

```html
<dez-global-svg nb-staffs="3" nb-measures="123" measures="measures.json"></dez-global-svg>
<dez-global-svg nb-staffs="3" nb-measures="123" measures="[{number: $n, measure: 'b/d'}*]"></dez-global-svg>
```

```html
<dez-global-svg
    nb-staffs="3"
    nb-measures="123"
    measures="
        [
            {number: 1, measure: '3/4'},
            {number: 23, measure: '6/8'},
            {number: 45, measure: '4/4'},
            {number: 89, measure: '3/4'}
        ]"
></dez-global-svg>

dez-canvas utilisé:

<dez-canvas
    bg="$svg-code"
    grid="[{bottom: $b, top: $t, divisions: (23-1)*3 + (45-23)*6/2 + (89-45)*4 +(123-89)*3 }*]"
></dez-canvas>
```

```html
<dez-global-svg
    nb-staffs="3"
    nb-measures="123"
    measures="['1 => 3/4', '23 => 6/8', '45 => 4/4', '89 => 3/4']"
></dez-global-svg>

<dez-global-svg
    nb-staffs="3"
    nb-measures="123"
    measures="1 => 3/4 ... 23 => 6/8 ... 45 => 4/4 ... 89 => 3/4"
></dez-global-svg>
```

**dez-wave**

Ce n'est qu'un composant d'affichage.

```html
<dez-wave image="wave.png" time="3:23" positions="wave-positions.json"></dez-wave>

dez-canvas utilisé:

<dez-canvas bg="wave.png" grid="grid.json"></dez-canvas>
```

```html
<dez-wave image="wave.png" time="3:23" positions="wave-positions.json"></dez-wave>
<dez-wave image="wave.png" time="3:23" positions="[[{x: $x, time: $s}*]*]"></dez-wave>
<!--positions graphiques de certains temps audio. On en aura généralement que deux, le premier et le dernier. -->

dez-canvas utilisé:

<dez-canvas bg="wave.png" grid="[{bottom: $b, top: $t, positions: [$x*]}*]"></dez-canvas>
```

dez-canvas manipule ici des positions audio (et non musicales), mais il n'en est pas au courant.

**dez-audio**

dez-audio


```html
<dez-audio audio="piece.mp3"></dez-audio>
<dez-audio audio="piece.mp3" synchro="synchro.json"></dez-audio>
<dez-audio audio="live.mp3" synchro="synchro2.json"></dez-audio>
<dez-audio audio="piece.mp3" synchro="[[{offset: $x, time: $s}*]*]"></dez-audio>
<!--correspondance entre certaines positions musicales et certaines positions audio. On peut en avoir à chaque beat... ou pas. Typiquement on peut très rapidement synchroniser à peu près un morceau en donnant que quelques points de synchro. -->
```

La synchro entre temps musical et temps audio n'est nécesaire que si on utilise à la fois dez-wave et dez-score (donc si on a une partition et un .mp3). L'appli peut fonctionner avec uniquement des dez-wave et dez-audio (temps audio partout, on n'a que du .mp3) ou uniquement des dez-score partout (on n'a qu'une partition).


**dez-svg**

(utilité ?)
dez-canvas utilisé:

```
<dez-canvas bg="$svg" grid="grid.json"></dez-canvas>
<!-- svg généré à la volée à partir du mp3|wav|ogg. besoin d'1 composant différent que dez-canvas?? -->
```
