
% StaffSymbol
% X: NonMusicalPaperColumn
%    X,Y: System
% Y: VerticalAxisGroup
%   X: NonMusicalPaperColumn
%      X,Y: System
%   Y: VerticalAlignment
%      X: NonMusicalPaperColumn
%         X,Y: System
%      Y: System


#(define (print-staff-pos grob)
   (
      let* (
         (system   (ly:grob-system grob))
         (y-extent (ly:grob-extent grob system Y))
	       (bottom   (car y-extent))
	       (top      (cdr y-extent))
      )
      (
         if (not (equal? (ly:grob-property grob 'transparent) #t)) (
	           format #t "\nStaff top and bottom (in staff-spaces):\n->  ~10,2f ~10,2f" top bottom
	       )
      )
   )
)

\layout {
  \context {
    \Score
    \once \override StaffSymbol.after-line-breaking = #print-staff-pos
  }
}
