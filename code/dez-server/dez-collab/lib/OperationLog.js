var Analysis = require('./Analysis')

class OperationLog {

	constructor () {
		this._log = []
		this._version = 0
		this._analysis = new Analysis()
	}

	addOperation (operation) {
		this._version++;
		operation.version = this._version;
		if (operation.op.action == "LOAD_NEW_ANALYSIS") {
			this._log = []
		}
		this._log.push(operation)
		this._updateAnalysis(operation)
		return operation
	}

	get operations () {
		return  this._log.filter(o => true)
	}

	from (version) {
			return this._log.slice(version-1)
	}

	get version () {
		return this._version
	}

	get analysisLabels () {
		return this._analysis.labels
	}

	_updateAnalysis (operation) {
		if (operation.op.action == "LOAD_NEW_ANALYSIS") {
			this._analysis.removeLabels()
			console.log(operation);
			for (let label of operation.op.labels) {
				this._analysis.addLabel(label)
			}
		} else if (operation.op.action == "REMOVE") {
			if (this._analysis.getLabel(operation.op.labelId) != undefined) {
				this._analysis.removeLabel(operation.op.labelId)
			}
		} else if (operation.op.action == "CREATE") {
			let newLabel = operation.op.changes
			newLabel.id = operation.op.labelId
			this._analysis.addLabel(newLabel)
		} else {
			let label = this._analysis.getLabelById(operation.op.labelId)
			Object.keys(operation.op.changes).forEach(key => {
				if (key != "id") {
					let value = operation.op.changes[key]
					if (key == "line") {
						let lineTab = value.split('.')
						if (lineTab[0] == 'staves') {
							label.staff = parseInt(lineTab[1])
						} else {
							delete label.staff
						}
					}
					label[key] = value
				}
			})
			this._analysis.replace(operation.op.labelId,label)
		}
	}

	generateAnalysisByOperation () {
		return this._analysis
	}

	containsLabel (id) {
		return this._analysis.getLabel(id) != null
	}

}

module.exports = OperationLog;
