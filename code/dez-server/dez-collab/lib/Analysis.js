class Analysis {

	constructor () {
		this._analysis = {}
	}

	addLabel (label) {
		this._analysis[label.id]= label
	}

	get labels () {
		let ret = []
		for (let key in this._analysis) {
			ret.push(this._analysis[key])
		}
		return ret
	}

	getLabelById (id) {
		let copy = {}
		let label = this._analysis[id]
		Object.keys(label).forEach(key => {
			copy[key] = label[key]
		})
		copy.end = label.end
		return copy
	}

	removeLabel (id) {
    delete this._analysis[id]
  }

	getLabel (id) {
		return this._analysis[id]
	}

	replace (id, label) {
		this.removeLabel(id)
		this.addLabel(label)
	}

	removeLabels() {
		this._analysis = {}
	}

}

module.exports = Analysis;
