let io = require('socket.io-client');

let url = "http://0.0.0.0:3000"
let options ={
  transports: ['websocket'],
  'force new connection': true
};

async function clientConnection () {
  let client = io.connect(url, options)
  return new Promise ((resolve) => {
    client.on('connect', () => {
      resolve(client)
    })
  })
}

async function initialisation (labels = []) {
  let init = new Client()
  await init.connect()
  init.emit('reinit')
  labels.forEach((l, i) => init.create(l, "op" + (i+1), "change" + (i+1)))
  init.disconnect()
}

async function getLabel (id) {
  let client = new Client()
  await client.connect()
  return new Promise ((resolve) => {
    client.on('analysis-channel', (data) => {
      client.disconnect()
      resolve(new Label(data.current_analysis.filter(l => l.id == id)[0]))
    })
    client.emit('analysis-demande', {})
  })
}

async function clientsCurrentlyMoveLabels (client1, client2, NB_MVT1, NB_MVT2) {
  let nb_mvt1 = 0
  let nb_mvt2 = 0
  let i = 1
  while (nb_mvt1 < NB_MVT1 || nb_mvt2 < NB_MVT2) {
    let turn = Math.floor(Math.random() * 2)
    if (turn == 0 && nb_mvt1 < NB_MVT1) {
      await client1.moves(2, "op1" + nb_mvt1, "change1" + nb_mvt1)
      nb_mvt1++
    } else if (turn == 1 && nb_mvt2 < NB_MVT2){
      await client2.moves(2, "op2" + nb_mvt2, "change2" + nb_mvt2)
      nb_mvt2++
    }
    i++
  }
}

class Client {
  constructor (sVersion = 0) {
    this.version = sVersion
    this._pendingOps = []
  }
  async connect () {
    this._client = await clientConnection()
  }
  async setLabel (id) {
    this.label = await getLabel(id)
  }
  set version (version) {
    this._sVersion = version
    this._version = this._sVersion
  }
  create (label) {
    this.label = label
    this._version++
    this._client.emit('create-label', {
      server_version: this._sVersion,
      client_version: this.version,
      op: label.currentOp("op1", "change1")
    })
  }
  validate (op) {
    this._pendingOps = this._pendingOps.filter(o => o.id != op.id)
  }
  emit (id, data = {}) {
    this._client.emit(id, data)
  }
  async _emit (op) {
    this._client.emit('update-label', {
      server_version : this._sVersion,
      client_version : this._version,
      op : op
    })
    return new Promise ((resolve) => {
      setTimeout(() => resolve(), 10);
    })
  }
  async moves (step, opId, changeId) {
    this._version++
    this.label.move(step)
    let op = this.label.currentOp (opId, changeId)
    this._pendingOps.push(op)
    await this._emit(op)
  }
  async multiMovesLabel (times, step, opId, changeId) {
    for (let i = 0; i < times; i++) {
      await this.moves(step, opId + i, changeId + i)
    }
  }
  async changeType (type, opId, changeId) {
    this._version++
    this.label.type = type
    await this._emit(this.label.currentOp (opId, changeId))
  }
  async resizes (step, opId, changeId) {
    this._version++
    this.label.resize(step)
    await this._emit(this.label.currentOp (opId, changeId))
  }
  async multiResizesLabel (times, step, opId, changeId) {
    for (let i = 0; i < times; i++) {
      this.resizes(step, opId + i, changeId + i)
    }
  }
  on (id, cb) {
    this._client.on(id, cb)
  }
  disconnect () {
    this._client.disconnect()
  }
}

class Label {
  constructor (data) {
    this._id = data.id
    this._type = data.type || "Pattern"
    this._tag = data.tag || ""
    this._start = data.start || 0
    this._duration = data.duration || 10
    this._staff = data.staff || 1
    this._line = data.line || "staves.1"
    this._action = "CREATE"
    this._changes = {
      type : this._type,
      tag : this._tag,
      start : this._start,
      duration : this._duration,
      staff : this._staff,
      line : this._line
    }
  }
  get id () { return this._id }
  set type (type) {
    this._type = type
    this._action = "UPDATE"
    this._changes = {type : this._type}
  }
  currentOp (opId, changeId) {
    let changes = { id : changeId }
    Object.keys(this._changes).forEach(k => changes[k] = this._changes[k])
    return {
      id : opId,
      labelId : this._id,
      action : this._action,
      changes : changes
    }
  }
  move (dx) {
    // only right move!
    this._start += dx
    this._action = "MOVE"
    this._changes = { start : this._start }
  }
  resize (dx) {
    // only right resize!
    this._duration += dx
    this._action = "RESIZE"
    this._changes = { duration : this._duration }
  }
}

module.exports = {
  clientConnection,
  initialisation,
  getLabel,
  clientsCurrentlyMoveLabels,
  Client,
  Label
}
