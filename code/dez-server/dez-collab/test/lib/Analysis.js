class Analysis {
  apply (data) {
    this._analysis = data
  }
  add (label) {
    this._analysis.push(label)
  }
  applyOperation (operation) {}
}

module.exports = Analysis
