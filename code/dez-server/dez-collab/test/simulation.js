const Label = require('./lib/Label')
const Client = require('./lib/Client')

let lines = [
  "top.1",
  "top.2",
  "top.3",
  "staves.1",
  "staves.2",
  "staves.3",
  "bot.1",
  "bot.2",
  "bot.3"
]

let types = [
  "S",
  "CS1",
  "CS2",
  "CS3",
  "Cadence",
  "Pattern",
  "Theme",
  "Tonality",
  "Modulation"
]

let mouvements = [
  "LEFT",
  "RIGHT",
  "TYPE",
  "LINE",
  // "ANALYSIS"
]

function generateScenario() {
  let ret = ""
  let nbMvts = 10 + Math.floor(Math.random()*100)
  for (let i = 0; i < nbMvts; i++) {
    let mouvement = mouvements[Math.floor(Math.random()*mouvements.length)]
    if (mouvement == "LEFT") {
      ret += "<"
    } else if (mouvement == "RIGHT") {
      ret += ">"
    } else if (mouvement == "TYPE") {
      ret += "t(" + types[Math.floor(Math.random()*types.length)] + ")"
    } else if (mouvement == "LINE") {
      ret += "l(" + lines[Math.floor(Math.random()*lines.length)] + ")"
    } else if (mouvement == "ANALYSIS") {
      ret += "a()"
    }
  }
  console.log(ret);
  return ret
}

async function scenario1 () {
  let client = new Client()
  // client.speed = 5
  // client.step = 0.001
  await client.connect()
  let label = new Label()
  client.create(label)
  await exec(
    client,
    label,
    generateScenario()
  )
  client.disconnect()
}

async function exec(client, label, scenario) {
  let i = 0
  function getParam() {
    let j = i
    while (scenario[j] != ")" && j < scenario.length) {
      j++
    }
    if (scenario[j] == ")") {
      d = i+2
      i = j
      return scenario.substring(d,j)
    } else {
      throw "mauvais format de scenario"
    }
  }
  while (i < scenario.length) {
    if (scenario[i] == ">") {
      await client.move(label).of(5)
    } else if (scenario[i] == "<") {
      await client.move(label).toTheLeft().of(5)
    } else if (scenario[i] == "l") {
      await client.move(label).toLine(getParam())
    } else if (scenario[i] == "t"){
      await client.change(label).toType(getParam())
    } else if (scenario[i] == "a"){
      // analysis = getParam()
      await client.changeAnalysis([])
    }
    i++
  }
}

async function scenario () {
  let client = new Client()
  // client.speed = 5
  // client.step = 0.001
  await client.connect()
  let label = new Label({line: "staves.3"})
  client.create(label)
  await exec(
    client,
    label, generateScenario()
  )
  client.disconnect()
}

async function main () {
  scenario1()
  for (let i = 0; i < 10; i++) {
    scenario()
  }
}

main()
