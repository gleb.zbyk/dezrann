const Ajv = require('ajv');

class AjvJsonFormatValidator {

  constructor () {
    this._ajv = new Ajv()
  }

  validates (input, schema) {
    return this._ajv.compile(schema)(input);
  }

}

module.exports = AjvJsonFormatValidator
