/*
  This file is part of Dezrann <http://www.dezrann.net>
  Copyright (C) 2016-2021 by Algomus Team at CRIStAL (UMR CNRS 9189, Université Lille),
  in collaboration with MIS (UPJV, Amiens) and LITIS (Université de Rouen).

  Dezrann is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Dezrann is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Dezrann.  If not, see <https://www.gnu.org/licenses/>.
*/

let express = require('express');
let app = express();
let bodyParser = require('body-parser')
let jwt = require('jsonwebtoken')
let fs = require('fs');
let crypto = require('crypto');

let groups = {}

const PORT = 9999
const SALT_PREFIX = "D3zr4nn"
const SALT_SUFFIX = "@utH!"
const USERS_FILE_NAME = "users.json"

function membersOf(group) {
  if (groups[group] == undefined) return []
  return groups[group].concat(
    groups[group].reduce((acc, name) => acc.concat(membersOf(name)), [])
  )
}

function groupsOf (user) {
  groups = JSON.parse(fs.readFileSync('groups.json', 'utf8'))
  return Object.keys(groups).filter(key => membersOf(key).includes(user))
}

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

function hash (password) {
  return crypto
  .createHash('sha256')
  .update(SALT_PREFIX + password + SALT_SUFFIX)
  .digest('hex')
}

function authOk (uid, password) {
  let users = JSON.parse(fs.readFileSync(USERS_FILE_NAME, 'utf8'))
  return users[uid] && users[uid].password == hash(password)
}

function tocken (uid) {
  return {
    access_token : jwt.sign({
      exp: Math.floor(Date.now() / 1000) + (60 * 60),
      sub: uid,
      groups: groupsOf(uid)
    }, fs.readFileSync('private.key', 'utf8'), {
      algorithm : "RS256"
    })
  }
}

app.get('/ping', (req, res) => {
  res.status(200).send("pong")
})

app.post('/authenticate', (req, res) => {
  if (authOk(req.body.uid, req.body.password)) {
    res.status(201).send(tocken(req.body.uid))
  } else {
    res.status(401).send("The username or password don't match")
  }
})

app.post('/changepw', (req, res) => {
  if (authOk(req.body.uid, req.body.oldpassword)) {
    // TODO: lock file for simultaneous change password requests
    let users = JSON.parse(fs.readFileSync(USERS_FILE_NAME, 'utf8'))
    users[req.body.uid].password = hash(req.body.newpassword)
    fs.writeFileSync(USERS_FILE_NAME, JSON.stringify(users, null, 2), 'utf8')
    res.status(201).send(tocken(req.body.uid))
  } else {
    res.status(401).send("Wrong old password")
  }
})

console.log('Starting server on port ' + PORT)
app.listen(PORT);
