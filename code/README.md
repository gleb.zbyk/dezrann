# Dezrann 

Installation and running instructions can be found at dez-client/dezrann/dev/index.html

Some details follow here.

# Utilisation de gulp

Lorsqu'il y a du code en commun entre le serveur et le client, ce code est
stocké dans le répertoire `code/lib`. Il faut ensuite utiliser `gulp` pour
formater ce code et le copier dans la partie cliente et la partie serveur.

## Installation de `gulp`

voir la doc à https://github.com/gulpjs/gulp/blob/v3.9.1/docs/getting-started.md

En résumé:

```bash
$ sudo npm install --global gulp-cli
$ npm install --save-dev gulp
```

Ajouter le plugin `gulp-rm-lines`:

```bash
$ npm install --save-dev gulp-rm-lines
```

## Lancer gulp

Se placer dans le répertoire `code` et lancer:

```
$ gulp
```

Cela lance un processus qui copie les fichiers sur le client et les serveurs
à chaque sauvegarde des fichiers sources.
