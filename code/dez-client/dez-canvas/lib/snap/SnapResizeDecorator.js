class SnapResizeDecorator {

    constructor (direction, element, point) {
        this._MARGIN = 2;
        this._WIDTH = 13;
        this._HEIGHT = 26;
        this._DIRECTION = direction;
        this._element = element;
        this._element.attr({
            fill: "black",
            stroke: "white",
            strokeWidth: "3px",
            opacity: "0.8",
            class: direction.toLowerCase() + "-resize-decorator"
        })
        this.redraw(point);
    }

    remove () { this._element.remove(); }

    get width () { return this._WIDTH * 2; }

    redraw (point) {
        let dirFactor = this._DIRECTION=="RIGHT"?1:-1;

        if (this._DIRECTION == "RIGHT")
            point.x += 1.5 ;
        
        this._topPoint = {
            x: point.x + dirFactor*this._MARGIN,
            y: point.y - this._HEIGHT/2
        };
        this._spikePoint = {
            x: point.x + dirFactor*(this._MARGIN + this._WIDTH),
            y: point.y
        };
        this._bottomPoint = {
            x: point.x + dirFactor*this._MARGIN,
            y: point.y + this._HEIGHT/2
        }
        this._element.attr({
            "d": "M" + this._topPoint.x    + "," + this._topPoint.y +
                 "L" + this._spikePoint.x  + "," + this._spikePoint.y +
                 "L" + this._bottomPoint.x + "," + this._bottomPoint.y
        })
    }

    get _left () {
        return this._DIRECTION=="RIGHT"
            ?this._topPoint.x-this._MARGIN/2
            :this._spikePoint.x;
    }

    get _right () {
        return this._DIRECTION=="RIGHT"
            ?this._spikePoint.x
            :this._topPoint.x+this._MARGIN/2;
    }

    contains (x) {
        return this._left <= x && x <= this._right;
    }

}
