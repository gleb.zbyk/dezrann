class Background {

    constructor (src) {
        this._image = new Image();
        this._src = src
    }

    _load () {
        if(this._src == undefined) {
            return Promise.reject("undefined image")
        }
        return new Promise ((resolve, reject) => {
            this._image.onload = () => resolve("success");
            this._image.onerror = () => reject("fail to load image " + this._src );
            this._image.src = this._src;
        });
    }

    async width () {
        if (this._image.naturalWidth) {
            return this._image.naturalWidth;
        }
        await this._load()
        return this._image.naturalWidth;
    }

    async height () {
        if (this._image.naturalHeight) {
            return this._image.naturalHeight;
        }
        await this._load()
        return this._image.naturalHeight;
    }

}
