/**
 * Class representing a GRaphical OBject (GROB).
 * It has got 2 properties :
 *  - x : the starting point
 *  - width : the width of the grob
 * It contains a shape that can be shown when the starting point or the width of
 * the grob changes.
 * It has several parents that are their containers.
 * It has transformation methods (moveTo, resize, select...) that modify the
 * shape.
 */
class Grob {

    /**
     * Grob construction needs a starting position (x). The witdh default value
     * is 0. The shape can be set afterwards. It add the grob to the mediator.
     * @param  {Number} x         starting position
     * @param  {Number} [width=0] width
     * @param  {Shape} shape      shape to show
     * @param  {GrobsMediator} mediator  needed when selecting to unselect all
     * others grobs
     */
    constructor (x, width = 0, shape, mediator, id) {
        if (x < 0) {
            throw "x must be positive"
        }
        this._x = x;
        this._width = width;
        this._parents = []
        this._shape = shape;
        this._mediator = mediator;
        this._mediator && this._mediator.add(this);
        this._selected = false;
        this._id = id;
    }

    get id () { return this._id }

    /**
     * Access to the starting position
     * @readonly
     * @return {Number} starting position
     */
    get x () { return this._x }

    /**
     * Convert if grid is set, otherwise identity.
     * @private
     * @param  {Number} x given position
     * @param  {Bool}     useGrid, converts only when this is true 
     * @return {Number}   converted position or identity
     */
    _convertX (x, useGrid = false) { return (useGrid && this._grid) ? this._grid.convert(x) : x ;}

    /**
     * Update starting position (conversion or identity)
     * @private
     * @param  {Number} x given position
     */
    _updateX (x, useGrid = false) { this._x = this._convertX(x, useGrid); }

    /**
    * Width of the grob
    * @public
    */
    get width () { return this._width }
    set width (width) {
        this._updateWidth(width)
    }

    /**
     * Convert if grid is set, otherwise identity.
     * Width is transformed into position (right edge) to be converted.
     * Warning: it depends to this._x that may produce side effects. this._x
     * must have the correct value before calling this method!
     * @private
     * @param  {Number} width given width
     * @return {Number}       converted width
     */
    _convertWidth(width, useGrid = false) {
        return (useGrid && this._grid) ? this._grid.convert(this._x+width)-this._x : width
    }

    /**
     * Update width grob (conversion or identity). When the direction is 'LEFT'
     * the starting position is modified so that the right edge of the grob
     * stays unchanged.
     * Warning: it depends to this._x that may produce side effects. this._x
     * must have the correct value before calling this method AND IT CAN BE
     * MODIFIED!
     * @private
     * @param  {Number} width               given width
     * @param  {String} [direction="RIGHT"] resizing direction.
     */
    _updateWidth (width, direction = "RIGHT", useGrid = false) {
        if (direction == "LEFT") {
            let right = this._x + this._width;
            let newX = this._convertX(right-width, useGrid)
            let newWidth = right - newX;
            if (newWidth >= this._shape.MIN_WIDTH) {
                this._x = newX;
                this._width = newWidth;
            }
        } else {
            let newWidth = this._convertWidth(width, useGrid);
            if (newWidth >= this._shape.MIN_WIDTH) this._width = newWidth;
        }
    }

    /**
    * Shape property. The grob can set the shape if it does not hve one or it
    * can change the shape.
    * @todo width might be change...
    * @public
    */
    get shape () { return this._shape }
    set shape (shape) {
        if (this._shape != undefined) {
            this._shape.remove()
        }
        this._shape = shape;
        if (this._selected) {
          this._shape.showAsSelected()
        }
    }

    /**
    * Add a parent (container: Line)
    * @public
    * @param {Object} parent the new container of the grob
    */
    addParent (parent) {
        if (this._parents.filter(item => item.equalTo(parent)).length == 0) {
            this._parents.push(parent);
        }
    }

    get sortedParentsIds () {
        return this._parents.map(parent => parent.id).sort((a,b)=>a-b)
    }

    get isOnlyOnOneParent () {
        return this.parentsCount == 1
    }

    get parentsCount () {
        return this._parents.length
    }

    /**
    * Delete a parent (container: Line)
    * @public
    * @param  {Object} parent parent to delete
    */
    deleteParent (parent) {
        this._parents = this._parents.filter(item => item != parent)
    }

    leavesFromAllParents () {
        this._parents.forEach(parent => parent.delete(this));
    }

    get topestParent () {
        return this._parents.reduce(
            (top, parent) => parent.top<top.top?parent:top,
            this._parents[0]
        )
    }

    get bottomestParent () {
        return this._parents.reduce(
            (bottom, parent) => parent.bottom>bottom.bottom?parent:bottom,
            this._parents[0]
        )
    }

    hasParentOn (y) {
        return this._parents.filter(parent => parent.on(y)).length > 0
    }

    higherThan(y) {
        return this.bottom < y;
    }

    lowerThan(y) {
        return this.top > y;
    }

    /**
    * Deduct the top from the topest parent.
    * @public
    * @return {Number} top of the grob
    */
    get top () {
        if (this._parents.length == 0) return 0
        return this._parents.reduce(
            (top, parent) => top>parent.top?parent.top:top,
            this._parents[0].top
        )
    }

    /**
    * Deduct the bottom from the bottomest parent.
    * @public
    * @return {Number} bottom of the grob
    */
    get bottom () {
        return this._parents.reduce(
            (bottom, parent) => bottom<parent.bottom?parent.bottom:bottom,
            0
        )
    }

    /**
    * @private
    * @return {Number} height of the grob
    */
    get _height () { return this.bottom - this.top }

    /**
    * Is the grob in a container (Line).
    * @public
    * @return {Boolean} true if the grob is on a Line
    */
    get onLine () { return this._parents.length > 0 }

    /**
    * set the grid
    * @public
    * @param  {GrobGrid} grid grid to place the grob
    */
    set grid (grid) {
        this._grid = grid;
    }

    /**
    * Resize the grob. Change the width of the grob. If direction is 'LEFT',
    * the starting position is modified too.
    * @public
    * @param  {Number} width               given width
    * @param  {String} [direction="RIGHT"] resizing direction
    */
    resize (width, direction = "RIGHT", useGrid = false) {
        this._updateWidth(width, direction, useGrid)
        this.show()
    }

    /**
    * Move the grob to the given position.
    * @public
    * @param  {Number} x given position to move to
    */
    moveTo (x, useGrid = false, updateWidthWithGrid = false) {
        this._updateX(x, useGrid);
        if (updateWidthWithGrid)
            this._updateWidth(this._width, "RIGHT", true)

        this.show();
    }


    /**
    * Refresh the grob, optionnaly sticking it to the grid
    * @public
    */
    refresh (useGrid = false) {
        this.moveTo(this._x, useGrid, useGrid)
    }

    /**
    * @public
    * @return {Boolean} true if grob is selected
    */
    get selected () { return this._selected; }

    /**
    * Select the grob. Change the display of the shape. Mediator is used to
    * unselect all others grobs.
    * @public
    */
    select () {
        if (!this._selected && this.onLine) {
            this._mediator.unselectAll();
            this._shape.showAsSelected();
            this._selected = true;
        }
    }

    /**
    * Unselect the grob. Change the display of the shape.
    * @public
    */
    unselect () {
        if (this._selected) {
            this._shape.showAsUnselected();
            this._selected = false;
        }
    }

    /**
     * Display the shape of the grob on the canvas.
     * @public
     */
    show () {
        if (this._height > 0) {
            this._shape.show(this._x, this.top, this._width, this._height)
        }
    }

    /**
     * Deep or content equality. Grob is equal to another if the have the same
     * starting position.
     * @todo same width, same shape
     * @public
     * @param  {Grob} grob    grob to compare with
     * @return {Boolean}      true if equality
     */
    equalTo (grob) {
        return this.x == grob.x
    }

    /**
     * @public
     * @param  {Number} x given position
     * @return {Boolean}  True if the given position is contained in the shape.
     */
    contains (x) {
        return this._shape.contains(x)
    }

    /**
     * Remove Shape and remove grob from all its parent.
     */
    remove () {
        this._shape.remove();
        this._parents.forEach(parents => parents.delete(this))
    }

}
