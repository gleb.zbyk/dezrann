/**
 * Class used by a grob to unselect all others when it is selected. There is a
 * unique instance for a canvas. All grobs have a reference to this instances.
 * It must have a reference to all grobs of a canvas.
 * @type {Array}
 */
class GrobsMediator {

    constructor () {
        this._grobs = []
    }

    add (grob) {
        this._grobs.push(grob)
    }

    unselectAll() {
        this._grobs.forEach(grob => grob.unselect());
    }

    remove (id) {
        this._grobs = this._grobs.filter(grob => grob.id != id);
    }

}
