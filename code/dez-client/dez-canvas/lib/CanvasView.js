/**
 * The view is used to define the way canvas is displayed. It use itself a view
 * that actually implements display method.
 * - display method: SVG or others
 * - handle Margins
 * - grob view creation
 * - event positions
 */
class CanvasView {

    constructor (view) {
        this._view = view
        this._bg;
        this._backgroundTop = 0;
    }

    get height () { return this._view.height }
    set height (height) { this._view.height = height }

    changeHeightIfNeeded (height) {
        if (this._view.height < height || (this._view.height > height && this._bg == undefined)) {
            this.height = height
        }
    }

    addSpaceAtTop (height) {
        this._view.shiftImage(height)
        this._backgroundTop += height;
    }

    setToMinHeight () {
        this._backgroundTop = 0;
        this._view.fitToImage();
    }

    async setBackground (src) {
        this._bg = new Background(src);
        try {
            this._view.width = await this._bg.width();
            let bgHeight = await this._bg.height()
            this.changeHeightIfNeeded(bgHeight)
            this._view.setImage(src, bgHeight)
            this._view.shiftImage(this._backgroundTop);
        } catch (err) {
            console.log('setBackground(' + src + '): ' + err)
        }
    }

    createEvent (event) {
        return new CanvasEvent(event, this._view)
    }

    showCursorOn (x) {
      this._view.setCursorOn(x)
    }

    deleteCursor () {
      this._view.deleteCursor()
    }

}
