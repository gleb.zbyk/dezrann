class Track {

    constructor (element, x, y, topMargin) {
        this._topMargin = topMargin
        this._ox = x;
        this._oy = y + this._topMargin;
        this._lastx = x;
        this._lastdx = 0
        this._lasty = this._oy;
        this._lastdy = 0
        this._element = element
    }

    moveTo(x, y, speed = "SLOW") {
        y += this._topMargin
        if (speed == "FAST") {
            this._dispatch("track", x, y, x - this._ox, y - this._oy);
            this._lastx = x;
            this._lasty = y;
            return this;
        }
        let dx = x - this._lastx;
        let dy = y - this._lasty;
        let stepx = 0;
        let stepy = 0;
        if (dx != 0) stepx = dx/Math.abs(dx);
        if (dy != 0) stepy = dy/Math.abs(dy);
        let loops = Math.max(Math.abs(dx),Math.abs(dy))
        for (let i = 0; i <= loops; i++) {
            if (i < Math.abs(dx)) {
                this._lastx += stepx;
                this._lastdx += stepx;
            }
            if (i < Math.abs(dy)) {
                this._lasty += stepy;
                this._lastdy += stepy;
            }
            this._dispatch("track", this._lastx, this._lasty, this._lastdx, this._lastdy)
        }
        return this;
    }

    stop() {
        this._dispatch("end", this._lastx, this._lasty, this._lastdx, this._lastdy)
    }

    _dispatch (state, x, y, dx, dy) {
        let event = new CustomEvent(
            "track",
            {
                "detail": {"state": state, "x": x, "y": y, "dx": dx, "dy": dy},
                "composed":true,
                "bubbles": true
            }
        );
        this._element.dispatchEvent(event);
    }

}

class GesturesListenerElement {


    constructor (element) {
      this._LINE_HEIGHT = 30
      this._INTERLINE = 2
      this._NB_TOP_LINES = 3
      this._SVG_TOP_MARGIN = 25
      this._element = element;
      this._topMargin = (this._LINE_HEIGHT + this._INTERLINE) * this._NB_TOP_LINES + this._SVG_TOP_MARGIN + this._element.getBoundingClientRect().top
    }

    trackAt (x, y) {
        return new Track(this._element, x, y, this._topMargin)
    }

    longTapAt (x, y, testCallback) {
      this._element.dispatchEvent(new CustomEvent(
        "down",
        {
          "detail": {"x": x, "y": y + this._topMargin},
          "composed":true,
          "bubbles": true
        }
      ))
      setTimeout(() => {
        testCallback();
      }, 400);
    }
}

function getLeftMargin(element) {
    let svgElement = element.shadowRoot.querySelector('svg');
    return svgElement.getBoundingClientRect().left;
}

function ensureImageIsLoaded (element, callback, ms = 0) {
    if (ms > 1000) {
        callback(element);
    }
    let imageElement = element.shadowRoot.querySelector('svg>image');
    if (imageElement != null) {
        console.log("done in " + ms + " ms!");
        callback(element)
    } else {
        setTimeout(function () {
            ensureImageIsLoaded(element, callback, ms + 1)
        }, 1);
    }
}
