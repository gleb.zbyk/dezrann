class LabelTypes {

    constructor () {
        this._types = {

            // Old types, for compatibility

            'structe': { 'color': '#ffff00', 'line': 'top.1', 'style': 'display:none' },
            'struct=': { 'color': '#ffff00', 'line': 'top.1', 'style': 'display:none'  },
            'ped': { 'color': '#335555', 'line': 'bot.1', 'style': 'display:none'  },
            'deg': { 'color': '#4400bb', 'line': 'bot.1', 'style': 'display:none'  },
            'h': { 'color': '#440000', 'line': 'bot.3', 'style': 'display:none'  },
            'seq': { 'color': '#ff8800', 'line': 'top.3', 'style': 'display:none'  },
            'seq:all': { 'color': '#ff8800', 'line': 'top.2', 'style': 'display:none'  },
            'seqnext': { 'color': '#ee7700', 'line': 'top.3', 'style': 'display:none'  },
            'seqnnext': { 'color': '#dd6600', 'line': 'top.3', 'style': 'display:none'  },
            'seqnnnext': { 'color': '#cc5500', 'line': 'top.3', 'style': 'display:none'  },
            'ext:top': { 'color': '#ff8800', 'line': 'bot.3', 'style': 'display:none'  },
            'ext:bot': { 'color': '#ff8800', 'line': 'bot.3', 'style': 'display:none'  },
            'pm': { 'color': '#22ff22', 'line': 'bot.3', 'style': 'display:none'  },

            // Displayed types (but not all)
            'S':   { 'color': '#ff0000' },
            'S-inv': { 'color': '#ff4444', 'style': 'display:none' },
            'S-inc': { 'color': '#ffaaaa', 'style': 'display:none' },

            'CS1': { 'color': '#00ff00' },
            'CS1a': { 'color': '#44ff44', 'style': 'display:none' },
            'CS1b': { 'color': '#22ff22', 'style': 'display:none' },
            'CS1-inc': { 'color': '#aaffaa', 'style': 'display:none' },

            'CS2': { 'color': '#0000ff' },
            'CS2a': { 'color': '#4444ff', 'style': 'display:none' },
            'CS2b': { 'color': '#2222ff', 'style': 'display:none' },
            'CS2-inc': { 'color': '#aaaaff', 'style': 'display:none' },

            'S2':  { 'color': '#aa6600', 'style': 'display:none' },
            'S3':  { 'color': '#aa0066', 'style': 'display:none' },

            'Pattern': { 'color': '#aa3300', 'line': 'bot.2', 'style': 'margin-top: 15px' },
            'Theme':   { 'color': '#aa0033', 'line': 'bot.2', 'style': 'margin-top: 15px'  },

            'Tonality':   { 'color': '#440088', 'line': 'bot.2', 'style': 'margin-top: 20px'  },
            'Modulation': { 'color': '#4400ee', 'line': 'bot.2' },
            'Harmony':    { 'color': '#4400bb', 'line': 'bot.1' },

            'Pedal':   { 'color': '#335555', 'line': 'bot.1', 'style': 'margin-top: 15px'  },
            'Cadence': { 'color': '#ff00ff', 'line': 'all' },

            'Harmonic sequence': { 'color': '#ff8800', 'line': 'bot.3', 'style': 'margin-top: 15px'  },
            'Texture': { 'color': '#22ff22', 'line': 'bot.3' }, // Unison, Homorythmy, Parallel Move

            'Structure': { 'color': '#ffff00', 'line': 'top.1', 'style': 'margin-top: 15px'  },

            'Positive Feedback': { 'color': '#99ff99', 'style': 'margin-top: 15px; font-style:italic' },
            'Feedback': { 'color': '#dddddd', 'style': 'font-style:italic' },
            'Negative Feedback': { 'color': '#ff9999', 'style': 'font-style:italic' },

            'Comment': { 'color': '#dddddd', 'line': 'bot.3', 'style': 'margin-top: 15px; font-style:italic'  },
            'Edmus': { 'color': '#dddddd', 'style': 'display:none', 'tags' : {} }
        };

        this._colors = [
          '255, 0, 0',    //'red',
          '255, 255, 0',  //'yellow',
          '0, 128, 0',    //'green',
          '0, 0, 255',    //'blue',
          '0, 255, 255',  //'aqua',
          '75, 0, 130',   //'indigo',
          '255, 192, 203',//'pink',
          '128, 0, 0',    //'maroon',
          '128, 0, 128',  //'purple',
          '255, 0, 255',  //'fuchsia',
          '0, 255, 0',    //'lime',
          '0, 0, 128',    //'navy',
          '128, 128, 0',  //'olive',
          '255, 165, 0',  //'orange',
          '0, 128, 128'   //'teal'
        ]

        // The edmus state for user tags and their colors

        // The index of an unused color
        // Colors with higher indices may be used
        this._freeColorIndex = 0
        // Tags with an associated color
        this._usedTags = []
        // Unremovable tags associated with the piece
        this._neededTags = []

        // The state for unknown types
        // Never used at the same time as edmus colors
        // Simpler, as new unknown types cannot be created during a session
        this._typesFreeColorIndex = 0;
        this._unknownTypeColors = {};
    }

    // Set all the currently used tags in an Edmus piece
    set usedTags (tags) {
      this._usedTags = tags
    }

    // Set the starting type of an edmus piece
    set neededTags (tags) {
      this._neededTags = tags
    }

    /**
    * Delete the unused tags for the given type.
    * Currently type is always edmus
    * Set this._freeColorIndex.
    *
    * @param {String} type : given type
    */
    _freeColors (type) {
      if (Object.keys(this._types[type].tags).length === 0) return
      let tags = this._neededTags.concat(this._usedTags)
      let tagsColors = {}
      tags.forEach(tag => {
        tagsColors[tag] = this._types[type].tags[tag]
      })
      this._types[type].tags = tagsColors
      this._setFreeColorIndex(type)
    }

    /**
    * Set the index of the first color unused in this._colors array
    *
    * @param {String} type : given type
    */
    _setFreeColorIndex (type) {
      this._freeColorIndex = 0
      let tags = this._neededTags.concat(this._usedTags)
      let tagsOfType = this._types[type].tags
      let usedColors = Object.keys(tagsOfType)
      .filter(tag => tagsOfType[tag] != undefined)
      .map(tag => tagsOfType[tag].color )
      for (let i = 0; i < this._colors.length; i++) {
        let color = this._colors[i]
        if (!usedColors.includes(color)) {
          this._freeColorIndex = i
          break
        }
      }
    }

    // Return the color object associated with a type
    // In the case of edmus, return the color object associated with a tag
    // If the type or tag doesn't exist, attempt to associate a new one
    colorOfType(typeTag) {
      let type = typeTag.type
      let tag = typeTag.tag

      // If the type exists
      if (this._knownType(type)) {
        // If in an Edmus session
        if (tag && this._types[type].tags) {
          // If the tag already has a color
          if (this._types[type].tags[tag]) {
            // Return said color
            return new Color(this._types[type].tags[tag].color)
          } else {
            // Associate new color
            this._freeColors(type)
            let ret = this._colors[this._freeColorIndex]
            this._types[type].tags[tag]={'color': ret}
            return new Color(ret)
          }
        }
        // Return type color
        return new Color(this._types[type].color);
      }

      // If type was never seen before
      if (this._associatedUnknownType(type) === false) {
        // If there are still unused colors
        if (this._typesFreeColorIndex < this._colors.length) {
          // Associate a new color
          this._unknownTypeColors[type] = this._colors[this._typesFreeColorIndex];
          this._typesFreeColorIndex++;
        } else {
          // Set the type to black
          this._unknownTypeColors[type] = '#000';
        }
      }
      // Return the type color
      return new Color(this._unknownTypeColors[type]);
    }

    // Create a new colored tag
    coloredTag (tag, type) {
      return {
        name: tag,
        color: this.colorOfType({ type: type, tag: tag })
      }
    }

    // Set the color of a tag
    // used for unknown purposes for Edmus in dez-analysis
    setColorOfType (typeTag, color) {
      if (typeTag.type != undefined) {
        if (this._types[typeTag.type].tags == undefined) {
          this._types[typeTag.type].tags = {}
        }
        if (this._types[typeTag.type].tags[typeTag.tag] == undefined) {
          this._types[typeTag.type].tags[typeTag.tag] = {}
        }
        this._types[typeTag.type].tags[typeTag.tag].color = (new Color(color)).rgb
      }
    }

    // Return the static hard-coded list of types
    get types () {
      return Object.keys(this._types);
    }

    // Return the static tags and their colors
    get typesNameColor () {
      return Object.keys(this._types).map(name => {
        let type = this._types[name];
        let rgb = new Color(type.color).rgb
        return {
          'name': name,
          'color': type.color,
          'rgb': rgb,
          'style': type.style
        }
      });
    }

    // Check if a static type exists
    _knownType (type) {
      return Object.prototype.hasOwnProperty.call(this._types, type);
    }

    // Check if a missing type was already associated with a color
    _associatedUnknownType (type) {
      return Object.prototype.hasOwnProperty.call(this._unknownTypeColors, type);
    }

    // Get the default line of a type
    defaultLineOf (type) {
        return (this._types[type] && this._types[type].line) || "bot.3";
    }

}
