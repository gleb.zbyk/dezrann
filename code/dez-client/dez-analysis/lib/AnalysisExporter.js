class AnalysisExporter {

  constructor (analysis) {
    this._fieldsToExport = [
      'type',
      'start',
      'duration',
      'actual-duration',
      'staff',
      'line',
      'tag',
      'comment',
      'layers'
    ]
    this._analysis = analysis
  }

  toString (sortedGroups = []) {
    let ret = "";

    // From https://stackoverflow.com/a/34890276
    function groupBy(xs, key) {
      return xs.reduce((rv, x) => {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    }

    let labelsLine = groupBy(this._analysis.labels, 'line')

    function lineToString (line) {
      return line + ": "
      + labelsLine[line]
      .sort((a,b) => a.start - b.start)
      .map(label => label.toString())
      .join(', ') + "\n"
    }

    sortedGroups.forEach(
      group => Object.keys(labelsLine)
      .filter(line => line.startsWith(group))
      .sort()
      .forEach(line => ret += lineToString(line))
    )

    Object.keys(labelsLine)
    .filter(
      line => !sortedGroups.reduce(
        (acc, group) => acc?acc:line.startsWith(group),
        false)
      )
      .sort()
      .forEach(line => ret += lineToString(line))

      return ret;
  }

  _cleanUpDuration (label) {
    if (label.duration == 0) delete label.duration
  }

  _cleanUpIterable (key, label) {
    if (!label[key] || label[key].length == 0) delete label[key]
  }

  _cleanUpTag (label) {
    this._cleanUpIterable("tag", label)
  }

  _cleanUpComment (label) {
    this._cleanUpIterable("comment", label)
  }

  _cleanUpLine (label) {
    if (typeof label.line === 'undefined' || label.line.startsWith('staves')) {
      if (typeof label.staff === 'undefined') {
        label.staff = parseInt(label.line.split('.')[1])
      }
      delete label.line
    } else {
      delete label.staff
    }
  }

  _cleanUpLayers (label) {
    this._cleanUpIterable("layers", label)
  }

  _cleanedUp (label) {
    let ret = Object.assign({}, label)
    this._cleanUpLayers(ret)
    this._cleanUpDuration(ret)
    this._cleanUpTag(ret)
    this._cleanUpComment(ret)
    this._cleanUpLine(ret)
    return ret
  }

  _labelsToExport () {
    return this._analysis.labels.map(label => this._cleanedUp(label))
  }

  toJson (layout) {
    let labelsJsonRaw = this._labelsToExport().map(item => {
      // No indentation here, we keep each label on a single line
      if (item.duration) {
        item["actual-duration"] = item.duration
        delete item.duration
      }
      if (item.ioi) {
        item.duration = item.ioi
      }
      if (layout && item.line == layout.lineOf(item)) {
        delete item.line
      }
      return JSON.stringify(item, this._fieldsToExport, " ").replace(/\n/g, "")
    })

    let indent = '    '
    return '[\n' + indent + indent + labelsJsonRaw.join(',\n' + indent + indent) + '\n' + indent + ']'
  }

  toCSV () {
    let fields = this._fieldsToExport
    return fields.join(";")
    + "\n"
    + this._labelsToExport().map(item => fields
      .map(field => item[field])
      .join(';')
    ).join('\n')
  }

}
