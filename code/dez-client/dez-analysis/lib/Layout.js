class Layout {

  constructor (data) {
      this._data = data
  }

  _matches (label, filter) {
    return filter.type == label.type
      && filter.layers.filter(l => !label.layers.includes(l)).length == 0
  }

  lineOf(label) {
    if (this._data) {
      for (const rule of this._data) {
        if (this._matches(label, rule.filter)) {
          return rule.style.line
        }
      }
    }
  }

}
