class Analysis {

  constructor () {
    this.erase()
    // ! HARD CODED !
    this._linesNames = [
      "staves.1",
      "top.3",
      "top.2",
      "top.1",
      "bot.1",
      "bot.2",
      "bot.3"
    ]
  }

  erase () {
    this._labels = []
  }

  get size () {
    return this._labels.length
  }

  get labels () {
    // return a copy not the private object
    return this._labels.filter(label => true)
  }

  addLabel (label) {
    if (label.line == undefined) {
      if (label.staff != undefined) {
        label.line = "staves." + label.staff.toString()
      } else {
        label.line = this._mostFreeLineAt(label.start, label.ioi)
        if (label.line.startsWith("staves")) {
          label.staves = parseInt(label.line.split('.')[1])
        }
      }
    }
    this._labels.push(label);
  }

  _mostFreeLineAt (start, ioi) {
    // TO OPTIMIZE
    for (let lineName of this._linesNames) {
      let overlaps = this._labels
      .filter(label => label.line == lineName)
      .filter(label => label.start <= start && start <= label.start + label.ioi)
      .length
      if (overlaps == 0) return lineName
    }
    return "staves.1"
  }

  get usedTags () {
    // OPTIMIZE: set list only on label modification
    let ret = []
    this._labels.forEach(label => {
      let tag = ret.filter(t => t.name == label.tag)[0]
      ret = ret.filter(t => t.name != label.tag)
      ret.push({
        name : label.tag,
        occurences : tag != undefined?tag.occurences + 1:1
      })
    })
    return ret
  }

  getLabelById (id) {
    let copy = {}
    let label = this._labels.filter(l => l.id == id)[0]
    if (label) {
      Object.keys(label).forEach(key => {
        // tests to delete when the good json format will be used
        if (key == "duration") {
          copy["actual-duration"] = label.duration
        } else if (key == "ioi") {
          copy.duration = label.ioi
        } else {
          copy[key] = label[key]
        }
      })
      copy.end = label.end
      return new Label(copy)
    } else {
      return undefined
    }
  }

  removeLabel (id) {
    this._labels = this._labels.filter(l => l.id != id)
  }

  updateLabel (label) {
    if (Object.keys(label).length == 0) return
    let savedLabel = this.getLabelById(label.id)
    if (savedLabel == undefined) return
    Object.keys(label).forEach(key => {
      if (label[key] != undefined) {
        savedLabel[key] = label[key]
      }
      if (label[key] == undefined && (key == "duration" || key == "ioi")) {
        savedLabel[key] = label[key]
      }
    })
    if (savedLabel.line.substring(0,6) != "staves") {
      delete savedLabel.staff
    }
    this.removeLabel(label.id)
    this.addLabel(savedLabel)
  }

  /**
   * Returns a score rougly matching the expected reading order of labels
   * @param {Label} label : label to score 
   */
  _scoreGenerator(label) {
    return (label.start * 1024  || 0) +
           (Number.parseFloat(label.staff) || 0) +
           (label.ioi / 1024 || 0);
  }

  /**
   * Returns the next label amongst labels matching a filter, according to a score generator
   * If no filter is passed, no filtering will be applied
   * If no selected label is passed, the lowest scored label will be returned
   * @param {Label} selected : The label against which labels will be filtered and compared  
   * @param {(label) => number} scoreGenerator : A function that generates a numerical value to sort labels by 
   * @param {(filteree, selected) => boolean } filter : A function to filter labels, receiving the filtered label and the selected label
   * @returns {Label | undefined} The next label according to the scoreGenerator, undefined if no labels are found or if selected is falsy 
   */
  getNextLabel(selected = undefined, filter = undefined, scoreGenerator = this._scoreGenerator) {
    let labels = this.labels;
    
    if (filter) {
      labels = this.labels.filter(label => { return filter(label, selected)});
    }

    if (selected) {
      const selectedScore = scoreGenerator(selected);
      labels = labels.filter(label => scoreGenerator(label) > selectedScore);
    }

    if (!labels.length) {
      return;
    }

    const lowestScoreLabel = labels.reduce(
      (a, b) => scoreGenerator(b) > scoreGenerator(a) ? a : b,
    );

    return lowestScoreLabel;
  }

  /**
   * 
   * Similar to getNextLabel, but inverses the score function for convenience
   */
  getPrevLabel(selected = undefined, filter = undefined, scoreGenerator = this._scoreGenerator) {
    return this.getNextLabel(selected, filter, l => -scoreGenerator(l));
  }
}
