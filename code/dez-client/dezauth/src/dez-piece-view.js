/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import {GestureEventListeners} from '@polymer/polymer/lib/mixins/gesture-event-listeners.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/av-icons.js';
import '@polymer/iron-icons/device-icons.js';
import '@polymer/iron-icons/image-icons.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-meta/iron-meta.js';
import './dez-acl.js';
import './shared-styles.js';
import './dez-global-variable.js';


class DezPieceView extends GestureEventListeners(PolymerElement) {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        div.card h1 a {
          text-decoration: none;
        }

        .tabs {
          display: flex;
        }

        .tab-item {
          flex-item: 0 0 auto;
          padding: 10px 15px;
          border-top: 1px solid #9E9E9E;
          border-left: 1px solid #9E9E9E;
          opacity: 0.6;
        }

        .tab-item:hover {
          cursor: pointer;
          opacity: 1;
        }

        .tab-item:first-child {
          border-top-left-radius: 5px;
        }

        .tab-item:last-of-type {
          border-top-right-radius: 5px;
          border-right: 1px solid #9E9E9E;
        }

        .tab-item.active {
          opacity: 1;
        }

        .analysis-view {
          margin: 20px 10px;
        }

        .analysis-content {
          color: black;
          border: 1px solid #9E9E9E;
          opacity: 0.6;
          padding: 20px;
        }

        .analysis-labels {
          border: 1px solid #EEE;
          border-radius : 5px;
          padding : 10px;
          height: 300px;
          overflow-y: scroll;
        }

      </style>

      <iron-meta id="config" key="config" value="{{config}}"></iron-meta>

      <div class="card">
        <h1>
          <iron-icon icon="image:music-note"></iron-icon>
          [[_currentPiece.title]]
        </h1>

        <p>[[_currentPiece.composer]]</p>
        <iron-icon icon="av:hearing" hidden$="[[!_currentPiece.sources.audios]]"></iron-icon>
        <template is="dom-repeat" items="[[_currentPiece.sources.images]]" as="image" mutable-data>
          <iron-icon icon="av:queue-music"></iron-icon>
        </template>
        <template is="dom-repeat" items="[[_currentPiece.sources.audios]]" as="audio" mutable-data>
          <template is="dom-repeat" items="[[audio.images]]" as="image" mutable-data>
            <iron-icon icon="device:graphic-eq" alt="hop"></iron-icon>
          </template>
        </template>
        <div class="analysis-view">
          <div class="tabs" id="analysesTabs">
            <template is="dom-repeat" items="[[_analyses]]" as="analysis" mutable-data>
              <div on-tap="_handleOnTap" class="tab-item">[[analysis]]</div>
            </template>
          </div>
          <div class="analysis-content" hidden$="[[_noAnalysis]]">
            <div class="analysis-labels">
              <pre>[[_currentLabels]]</pre>
            </div>
            <dez-acl acl="[[_currentAnalysis.access]]"></dez-acl>
          </div>
        </div>
        <dez-acl acl="[[_currentPiece.access]]"></dez-acl>
      </div>

      <iron-ajax
        id="pieceLocationRequest"
        url="[[_pieceUrl]]"
        handle-as="json"
        debounce-duration="300"
        auto
        on-response="_pieceReady"
        on-error="_handleAjaxError">
      </iron-ajax>

      <iron-ajax
        id="analysesLocationRequest"
        url="[[_analysesUrl]]"
        handle-as="json"
        debounce-duration="300"
        auto
        on-response="_analysesReady"
        on-error="_handleAjaxError">
      </iron-ajax>

      <iron-ajax
        id="analysisLocationRequest"
        url="[[_analysisUrl]]"
        handle-as="json"
        debounce-duration="300"
        auto
        on-response="_analysisReady"
        on-error="_handleAjaxError">
      </iron-ajax>


      <app-location route="{{route}}"></app-location>
      <app-route route="{{route}}" pattern="/:page" tail="{{tail}}" on-tail-changed="_handleTailChanged">
      </app-route>

      <dez-global-variable key="userData" value="{{storedUser}}"></dez-global-variable>

    `;
  }

  static get properties() {
    return {
      storedUser: {
        type : Object,
        observer : "_handleStoredUserChanged"
      }
    };
  }

  _handleStoredUserChanged () {
    if (this.storedUser) {
      let bearer = 'Bearer ' + this.storedUser.access_token
      this.$.pieceLocationRequest.headers['Authorization'] = bearer
      this.$.pieceLocationRequest.generateRequest()
      this.$.analysesLocationRequest.headers['Authorization'] = bearer
      this.$.analysesLocationRequest.generateRequest()
      this.$.analysisLocationRequest.headers['Authorization'] = bearer
      this.$.analysisLocationRequest.generateRequest()
    }
  }

  _pieceReady (event) {
    this._analyses = []
    this._currentAnalysis = ""
    this._noAnalysis = true
    this._currentPiece = event.detail.response;
    this._analysesUrl = this._pieceUrl + "/analyses"
  }

  _analysesReady (event) {
    let response = event.detail.response
    if (response) {
      this._analyses = response.map(name => name.split('.')[0]).filter(name => name != "empty")
      if (this._analyses.length > 0) {
        this._updateCurrentAnalysis (this._analyses[0])
      }
    }
  }

  _handleOnTap (event) {
    this._updateCurrentAnalysis (event.model.analysis)
  }

  _updateCurrentAnalysis (name) {
    this.$.analysisLocationRequest.headers['Authorization'] = this.storedUser?'Bearer ' + this.storedUser.access_token:""
    this._analysisUrl = this._analysesUrl + "/" + name + ".dez"
    Array.from(this.$.analysesTabs.children)
    .filter(item => item.className.includes("tab-item"))
    .forEach(item => {
      item.className = "tab-item"
      if (item.innerText === name) {
        item.className += " active"
      }
    })
  }

  _analysisReady (event) {
    this._noAnalysis = false
    this._currentAnalysis = event.detail.response
    this._currentLabels = JSON.stringify(event.detail.response.labels, null, 2)
  }

  _handleTailChanged (event) {
    let path = event.detail.value.path
    this._pieceUrl = JSON.parse(this.config).dezws + path
  }

  _handleAjaxError (event) {
    let response = event.detail.request.xhr.response
    if (response && response.name == "TokenExpiredError") {
      this.storedUser = null
      this.set('route.path', '/login');
    }
  }


}

window.customElements.define('dez-piece-view', DezPieceView);
