class ScoreView {

    constructor (canvas, map, labelTypes) {
        this._canvas = canvas;
        this._map = map;
        this._labelTypes = labelTypes;
        this._toLabelId = {}
        this._toGrobId = {}
    }

    set labelTypes (labelTypes) {
      this._labelTypes = labelTypes
    }

    set map (map) {
        this._map = map
    }

    _id2index (id) {
        return parseInt(id.split('.')[1])
    }

    _id2group(id) {
        return id.split('.')[0]
    }

    remove (labelId) {
        this._canvas.deleteGrob(this._toGrobId[labelId]);
    }

    draw (label) {
        let id = this._canvas.add({
            x: this._map.toGraphicalX(label.start),
            width: this._map.toGraphicalLength(label.start, label.genericDuration, label.ioiMode),
            text: label.tag,
            color: this._labelTypes.colorOfType({
              type: label.type,
              tag: label.tag
            }).hex,
            shape: label.genericDuration==0?"VerticalLine":"Rectangle"
        })
        .onLine(label.line)
        .getId();
        this._toLabelId[id] = label;
        this._toGrobId[label.id] = id;
    }

    redraw (label) {
        this._canvas.updateGrob({
            id: this._toGrobId[label.id],
            text: label.tag == undefined ? "" : label.tag,
            color: this._labelTypes.colorOfType({
              type: label.type,
              tag: label.tag
            }).hex,
            width: this._map.toGraphicalLength(label.start, label.genericDuration, label.ioiMode),
            x: this._map.toGraphicalX(label.start),
            line: label.line
        })
    }

    selected (label) {
      this._canvas.selectGrob(this._toGrobId[label.id])
    }

    associateIds (label, grobId) {
      this._toLabelId[grobId] = label;
      this._toGrobId[label.id] = grobId;
    }

    toLabel (grob) {
        let label = this._toLabelId[grob.id]
        if (label == undefined) {
          label = {}
        }
        this.updateLabel(label, grob)
        return label;
    }

    updateLabel(label, grob) {
        label.start = this._map.toMusicalOnset(grob.x);
        if (grob.width == 0) {
          label.ioi = undefined
          label.duration = undefined
        } else {
          label.genericDuration = this._map.toMusicalDuration(
            grob.x,
            grob.width,
            label.ioiMode!=undefined?label.ioiMode:true
          )
        }
        label.tag = grob.text;
        if (grob.linesIds.length == 1) {
            label.line = grob.linesIds[0]
            if (this._id2group(label.line) == "staves") {
              label.staff = this._id2index(label.line)
            }
        } else { // 0 or several
            label.line = "all";
        }
    }

    resetLabels() {
        this._canvas.deleteGrobs();
    }

    deleteLines() {
        this._canvas.deleteLines();
    }
}
